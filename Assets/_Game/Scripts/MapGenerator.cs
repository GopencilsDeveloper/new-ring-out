﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PaintIn3D;
#if UNITY_EDITOR
using UnityEditor;
#endif
using PaintIn3D.Examples;

[ExecuteInEditMode]
public class MapGenerator : MonoBehaviour
{
    [Header("Inputs")]
    public bool createPrefab;
    public List<Mesh> meshNormalList;
    public List<Mesh> meshFixedList;
    public List<Sprite> artsSpriteList;
    // public List<Sprite> previewsSpriteList;

    [NaughtyAttributes.ReorderableList]
    public List<string> namesList;

    [Header("Map")]
    public Transform mapsParent;
    public Map mapPrefab;


    [NaughtyAttributes.Button]
    public void Generate()
    {
        for (int i = 0; i < meshNormalList.Count; i++)
        {
            Map map = Instantiate(mapPrefab, Vector3.zero, Quaternion.identity, mapsParent);
            map.gameObject.name = "Map " + meshNormalList[i].name;

            map.mapArtSprite = artsSpriteList[i];
            // map.mapPreview = previewsSpriteList[i];
            map.mapName = namesList[i].ToUpper();

            map.pipeMeshFilter.mesh = meshFixedList[i];
            map.channelCounter.Mesh = meshNormalList[i];
            map.pipe.AddComponent<MeshCollider>();
            map.pipePreviewMeshFilter = map.pipe.transform.GetChild(0).GetComponent<MeshFilter>();
            map.pipePreviewMeshFilter.mesh = meshFixedList[i];

            Vector3 startPos = meshNormalList[i].vertices[0];
            map.latch.position = startPos;

#if UNITY_EDITOR

            if (!createPrefab)
            {
                return;
            }

            string localPath = "Assets/_Game/Prefabs/ROPE DONE 1/" + map.name + ".prefab";

            // Make sure the file name is unique, in case an existing Prefab has the same name.
            localPath = AssetDatabase.GenerateUniqueAssetPath(localPath);

            // Create the new Prefab.
            PrefabUtility.SaveAsPrefabAssetAndConnect(map.gameObject, localPath, InteractionMode.UserAction);
#endif

        }
    }

}
