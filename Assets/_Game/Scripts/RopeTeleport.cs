﻿using System;
using System.Collections;
using System.Collections.Generic;
using Obi;
using UnityEngine;

public class RopeTeleport : MonoBehaviour
{
    public ObiActor actor;
    public MeshRenderer meshRenderer;

    public void Teleport(Transform trans)
    {
        StartCoroutine(C_Teleport(trans));
    }

    public void InitializeRopeAt(Transform tf)
    {
        StartCoroutine(C_InitializeRopeAt(tf));
    }

    private IEnumerator C_InitializeRopeAt(Transform tf)
    {
        yield return new WaitForFixedUpdate();
        meshRenderer.enabled = false;
        actor.enabled = false;
        actor.transform.position = tf.position;
        var rot = tf.rotation.eulerAngles;
        actor.transform.rotation = Quaternion.Euler(rot);

        yield return new WaitForFixedUpdate();
        actor.enabled = true;

        yield return actor.GeneratePhysicRepresentationForMesh();
        actor.ResetActor();
        actor.AddToSolver(null);
        
        yield return new WaitForSeconds(0.1f);
        meshRenderer.enabled = true;
    }

    IEnumerator C_Teleport(Transform trans)
    {
        yield return new WaitForFixedUpdate();
        actor.ResetActor();
        actor.enabled = false;
        actor.transform.position = trans.position;
        var rot = trans.rotation.eulerAngles;
        rot.z += 90f;
        actor.transform.rotation = Quaternion.Euler(rot);
        yield return new WaitForFixedUpdate();
        actor.enabled = true;

        // actor.enabled = false;

        // actor.transform.position = trans.position;
        // var rot = trans.rotation.eulerAngles;
        // rot.z += 90f;
        // actor.transform.rotation = Quaternion.Euler(rot);

        // yield return new WaitForFixedUpdate();
        // actor.enabled = true;


    }
}
