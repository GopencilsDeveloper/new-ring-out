﻿using System.Collections;
using System.Collections.Generic;
using Obi;
using PaintIn3D;
using UnityEngine;

public class P3DHitAtContact : P3dHitConnector
{
    public Vector3 offset;
    public float minDelta = 0.01f;
    ObiSolver solver;

    void Start()
    {
        solver = InputManager.Instance.solver;
        solver.OnCollision += Solver_OnCollision;
    }

    void OnDestroy()
    {
        solver.OnCollision -= Solver_OnCollision;
    }

    Vector4 lastContact;
    void Solver_OnCollision(object sender, Obi.ObiSolver.ObiCollisionEventArgs e)
    {
        Oni.Contact[] contacts = e.contacts.Data;
        for (int i = 0; i < e.contacts.Count; ++i)
        {
            Oni.Contact c = contacts[i];
            // make sure this is an actual contact:
            if (c.distance < 0.01f)
            {
                if ((c.point - lastContact).sqrMagnitude >= minDelta)
                {
                    lastContact = c.point;
                    DispatchPaintEventAt(new Vector3(c.point.x, c.point.y, c.point.z));
                    break;
                    // get the collider:
                    // Collider collider = ObiCollider.idToCollider[c.other] as Collider;

                    // if (collider != null){
                    //     DispatchPaintEventAt(new Vector3(c.point.x, c.point.y, c.point.z));
                    // 	// Debug.LogErrorFormat("Collider {0}, contact at {1}, num contact {2}, numEvent {3}", collider.name, c.point, e.contacts.Count, ++numEvent);
                    // 	// make it blink:
                    // 	// Blinker blinker = collider.GetComponent<Blinker>();

                    // 	// if (blinker)
                    // 	// 	blinker.Blink();
                    // }
                }
            }
        }
    }
    public void DispatchPaintEventAt(Vector3 pos){
        DispatchHits(false, null, pos, Quaternion.identity, 1, this);
    }
}
