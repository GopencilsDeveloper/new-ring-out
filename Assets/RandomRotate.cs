﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Obi;

public class RandomRotate : MonoBehaviour
{
    public float delay = 2;
    public float spacingTime = 0.8f;
    public float rotateTime = 1;
    // Start is called before the first frame update
    IEnumerator Start()
    {
        yield return new WaitForSeconds(delay);
        StartCoroutine(C_AutoRotate());
    }

    public ObiSolver solver;

    Quaternion lastAngle, targetAngle;
    IEnumerator C_AutoRotate()
    {
        while (true)
        {
            targetAngle = Random.rotation;
            lastAngle = transform.rotation;
            float elapsed = 0;
            while(elapsed < rotateTime){
                elapsed += Time.deltaTime;
                transform.rotation = Quaternion.Lerp(lastAngle, targetAngle, elapsed / rotateTime);
                var param = solver.parameters;
                param.gravity = -transform.up;
                solver.parameters = param;
                Oni.SetSolverParameters(solver.OniSolver,ref param);
                yield return null;
            }

            yield return new WaitForSeconds(spacingTime);
        }
    }
}
