﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Pool<T> : MonoBehaviour where T : Component
{
    [SerializeField] private T prefab;
    [SerializeField] private Transform parent;
    public static Pool<T> Instance { get; private set; }
    private List<T> pool = new List<T>();

    private void Awake()
    {
        Instance = this;
    }

    public T GetFromPool()
    {
        T instance;
        int lastIndex = pool.Count - 1;
        if (lastIndex >= 0)
        {
            instance = pool[lastIndex];
            instance.gameObject.SetActive(true);
            instance.transform.SetParent(parent, false);
            pool.RemoveAt(lastIndex);
        }
        else
        {
            instance = Instantiate(prefab);
            instance.transform.SetParent(parent, false);
        }
        return instance;
    }

    public void ReturnToPool(T objectToPool)
    {
        pool.Add(objectToPool);
        objectToPool.gameObject.SetActive(false);
    }
}
