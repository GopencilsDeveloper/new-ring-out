﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoSingleton<InputManager>
{
    private const float GRAVITY = 12.2625f; //125%
    public float minDragToRotate = 0.001f;
    public Transform cameraParent;
    public new Camera camera;
    public Obi.ObiSolver solver;
    public Vector2 rotateSpeed = new Vector2(10, 30);

    /// Flag set to true if the user currently makes an rotation gesture, otherwise false
    private bool rotating = false;
    /// The squared rotation width determining an rotation
    public const float TOUCH_ROTATION_WIDTH = 1; // Always
    /// The threshold in angles which must be exceeded so a touch rotation is recogniced as one
    public const float TOUCH_ROTATION_MINIMUM = 1;
    /// Start vector of the current rotation
    Vector2 startVector = Vector2.zero;


    public void Reset()
    {
        UpdateGravity();
    }

    Vector3 lastGrav = Vector3.zero;
    private void UpdateGravity()
    {
        var grav = -cameraParent.up;
        if (grav != lastGrav)
        {
            lastGrav = grav;
            var param = solver.parameters;
            param.gravity = -cameraParent.up * GRAVITY;
            solver.parameters = param;
            Oni.SetSolverParameters(solver.OniSolver, ref param);
        }
    }

    public void UpdateInputInGame()
    {
        UpdateGravity();
    }

    /// Processes input for touch rotation, only the first two touches are used
    private void TouchRotation()
    {
        if (Input.touchCount == 2)
        {
            if (!rotating)
            {
                startVector = Input.touches[1].position - Input.touches[0].position;
                rotating = startVector.sqrMagnitude > TOUCH_ROTATION_WIDTH;
            }
            else
            {
                Vector2 currVector = Input.touches[1].position - Input.touches[0].position;
                float angleOffset = Vector2.Angle(startVector, currVector);

                if (angleOffset > TOUCH_ROTATION_MINIMUM)
                {
                    // Vector3 LR = Vector3.Cross(startVector, currVector); // z > 0 left rotation, z < 0 right rotation
                    cameraParent.Rotate(Vector3.forward, angleOffset * rotateSpeed.y);
                    UpdateGravity();
                    // if (LR.z > 0)
                    //     mouseLook.y += angleOffset;
                    // else if (LR.z < 0)
                    //     mouseLook.y -= angleOffset;

                    // mouseLook.y = Mathf.Clamp(mouseLook.y, 0, 180F); // Clamp looking down and up

                    // GameController.Instance.mainCamera.transform.localRotation = Quaternion.AngleAxis(-mouseLook.y, Vector3.right);
                    // startVector = currVector;
                }
            }
        }
        else
            rotating = false;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawLine(Vector3.zero, -cameraParent.up * GRAVITY);
    }

}
