﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ring : MonoBehaviour
{
    public Obi.ObiRope rope;
    public PaintIn3D.P3dPaintSphere paintSphere;
    public Obi.ObiStitcher stitcher;
    public Obi.ObiRope viewRope;
    /// <summary>
    /// Start is called on the frame when a script is enabled just before
    /// any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        rope.Solver = InputManager.Instance.solver;
        viewRope.Solver = InputManager.Instance.solver;
        stitcher.AddStitch(0,0);
        stitcher.AddStitch(2,2);
        // stitcher.AddStitch(5,5);
        stitcher.AddToSolver(null);
    }

}
