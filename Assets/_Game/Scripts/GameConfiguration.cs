﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameConfiguration : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS
    
    public float speedRotate;
    public float delayRotate;

    #endregion

    #region PARAMS
    #endregion

    #region PROPERTIES
    public static GameConfiguration Instance { get; private set; }
    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void Awake()
    {
        Instance = this;
    }

    #endregion

    #region DEBUG
    #endregion

}
