﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using UnityEditor;
using PaintIn3D;
using Obi;

public class Test : MonoBehaviour
{
    // public List<Map> mapList;
    // public List<Sprite> artList;
    // public List<Sprite> previewList;

    // [Button]
    // public void ReplaceArtAndPreview()
    // {
    //     for (int i = 0; i < mapList.Count; i++)
    //     {
    //         Map map = mapList[i];
    //         map.mapArtSprite = artList[i];
    //         map.mapPreview = previewList[i];
    //     }
    // }


    // [Space(40)]
    public List<Map> mapOldList;
    // public GameObject RingObj;
    public ObiCollisionMaterial obiCollisionMaterial;
    // public GameObject pipePreview;
    public GameObject previewCamera;

    [Button]
    public void UpdateMap()
    {
        for (int i = 0; i < mapOldList.Count; i++)
        {
            Map map = mapOldList[i];
            map.latch.localPosition = map.ring.localPosition;
            map.latch.localRotation = map.ring.localRotation;
            // GameObject pipePreviewInstance = Instantiate(pipePreview, Vector3.zero, Quaternion.identity, map.pipe.transform);
            // pipePreviewInstance.GetComponent<MeshFilter>().sharedMesh = map.pipe.GetComponent<MeshFilter>().sharedMesh;

            // GameObject previewCameraInstance = Instantiate(previewCamera);
            // previewCameraInstance.transform.SetParent(map.transform);
            // previewCameraInstance.transform.localPosition = new Vector3(0f, 0f, -5f);
            // previewCameraInstance.transform.localRotation = Quaternion.identity;
            // map.transform.localScale = new Vector3(8f, 8f, 8f);
            // map.ring = map.transform.GetChild(3);
            // map.transform.GetChild(0).GetComponent<Camera>().orthographicSize = 15f;
            // DestroyImmediate(map.ring.GetComponent<Rigidbody>());
            // DestroyImmediate(map.ring.GetComponent<Scanner>());
            // map.transform.GetChild(2).gameObject.AddComponent<ObiCollider>();
            // map.pipe.GetComponent<P3dPaintableTexture>().Color = new Color(1f, 1f, 1f, 0f);
            // map.transform.GetChild(3).GetChild(0).gameObject.SetActive(false);
            // map.pipe.GetComponent<ObiCollider>().CollisionMaterial = obiCollisionMaterial;

            // map.transform.GetChild(1).localPosition = map.transform.GetChild(3).localPosition;
            // map.transform.GetChild(1).localRotation = map.transform.GetChild(3).localRotation;
            // map.ring = map.transform.GetChild(3);

            // GameObject temp = Instantiate(RingObj);
            // temp.transform.SetParent(map.transform.GetChild(3));
            // temp.transform.localPosition = Vector3.zero;
            // temp.transform.localRotation = Quaternion.identity;

            // string localPath = "Assets/_Game/Prefabs/ROPE DONE/" + map.name + ".prefab";

            // // Make sure the file name is unique, in case an existing Prefab has the same name.
            // localPath = AssetDatabase.GenerateUniqueAssetPath(localPath);

            // // Create the new Prefab.
            // PrefabUtility.SaveAsPrefabAssetAndConnect(map.gameObject, localPath, InteractionMode.UserAction);
        }
    }

    [Button]
    public void SaveToPrefabs()
    {
        for (int i = 0; i < mapOldList.Count; i++)
        {
            Map map = mapOldList[i];


            // string localPath = "Assets/_Game/Prefabs/ROPE DONE 1/" + map.name + ".prefab";

            // Make sure the file name is unique, in case an existing Prefab has the same name.
            // localPath = AssetDatabase.GenerateUniqueAssetPath(localPath);
            // AssetDatabase.SaveAssets();

            // Create the new Prefab.
            // PrefabUtility.SaveAsPrefabAssetAndConnect(map.gameObject, localPath, InteractionMode.UserAction);
        }
    }

}
