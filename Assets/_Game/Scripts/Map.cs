﻿using System.Collections;
using System.Collections.Generic;
using PaintIn3D.Examples;
using UnityEngine;
using Obi;

public class Map : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS
    public GameObject pipe;
    public MeshFilter pipeMeshFilter;
    public MeshFilter pipePreviewMeshFilter;
    public P3dChannelCounter channelCounter;
    public Transform ring;
    public Transform latch;

    #endregion

    #region PARAMS

    public string mapName;
    public Art artPrefab;
    public Sprite mapArtSprite;
    // public Sprite mapPreview;
    public bool canRotate;

    public Vector3 startRot;
    #endregion

    #region PROPERTIES
    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void OnEnable()
    {
        Refresh();
    }

    void Update()
    {
        if (canRotate)
        {
            // Control();
            InputManager.Instance.UpdateInputInGame();
        }
    }

    public void Refresh()
    {
        canRotate = false;
        latch.GetComponent<BoxCollider>().isTrigger = true;
        startRot = transform.localRotation.eulerAngles;
    }

    [NaughtyAttributes.Button]
    public void Active()
    {
        StartCoroutine(C_Active());
    }

    IEnumerator C_Active()
    {
        canRotate = false;
        yield return new WaitForSeconds(GameConfiguration.Instance.delayRotate);
        // scanner.DelayFall(0);
        yield return new WaitForSeconds(0.5f);
        MapManager.Instance.ropeTeleport.gameObject.SetActive(true);
        MapManager.Instance.ropeTeleport.InitializeRopeAt(ring);
        yield return new WaitForSeconds(1.5f);
        latch.GetComponent<BoxCollider>().isTrigger = false;
        latch.gameObject.AddComponent<ObiCollider>();
    }

    // int counter = 0;

    // private void Control()
    // {
    //     if (Input.GetMouseButtonDown(0))
    //     {
    //         currentTouchPos = lastTouchPos = Input.mousePosition;
    //     }
    //     else
    //     if (Input.GetMouseButton(0))
    //     {
    //         currentTouchPos = Input.mousePosition;

    //         if (counter == 0)
    //         {
    //             directionTouch = Vector3.zero;
    //             counter = 1;
    //         }
    //         else
    //         {
    //             directionTouch = currentTouchPos - lastTouchPos;
    //             directionTouch.x = Mathf.Clamp(directionTouch.x, -150f, 150f);
    //             directionTouch.y = Mathf.Clamp(directionTouch.y, -200f, 200f);
    //         }



    //         lastTouchPos = currentTouchPos;
    //         transform.Rotate(
    //             new Vector3(directionTouch.y * 0.1f * GameConfiguration.Instance.speedRotate,
    //                         -directionTouch.x * 0.1f * GameConfiguration.Instance.speedRotate, 0f), Space.World);
    //     }
    //     else
    //     if (Input.GetMouseButtonUp(0))
    //     {
    //         directionTouch = Vector3.zero;
    //     }
    // }

    [NaughtyAttributes.Button]
    public void SpawnArt()
    {
        Art art = Instantiate(artPrefab);

        art.Initialize(mapArtSprite);
        art.OnMapCompleted();

        MapManager.Instance.currentArt = art;
    }

    // IEnumerator C_SpawnArt()
    // {

    // }

    // [NaughtyAttributes.Button]
    // public void SetRingRef()
    // {
    //     latch.localPosition = ring.localPosition;
    //     latch.localRotation = ring.localRotation;
    // }

    #endregion

    #region DEBUG
    #endregion

}
