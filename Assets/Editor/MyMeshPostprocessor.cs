﻿using UnityEditor;

public class MyMeshPostprocessor : AssetPostprocessor
{
    private void OnPreprocessModel()
    {
        ModelImporter modelImporter = assetImporter as ModelImporter;
        if (modelImporter != null)
        {
            if (assetPath.Contains("Pipe"))
            {
                modelImporter.importAnimation = false;
                modelImporter.importMaterials = false;
            }
        }
    }
}
