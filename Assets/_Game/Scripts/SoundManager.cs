﻿using System.Collections;
using System.Collections.Generic;
using Hellmade.Sound;
using UnityEngine;

public class SoundManager : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS
    [Header("Sources")]
    public AudioClip clap;
    public AudioClip finishPercent;
    public AudioClip rotate2Rounds;
    public AudioClip endWinFX;
    #endregion

    #region PARAMS
    #endregion

    #region PROPERTIES
    public static SoundManager Instance { get; private set; }
    #endregion

    #region EVENTS
    #endregion

    #region METHODS
    private void Awake()
    {
        Instance = this;
    }
    #endregion

    #region DEBUG
    #endregion

}
