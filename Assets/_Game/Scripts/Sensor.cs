﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PaintIn3D;

public class Sensor : MonoBehaviour
{

    public P3dHitRay hitRay;
    public bool canSense;
    // Vector3 lastPos;

    private void LateUpdate()
    {
        // if ((transform.position - lastPos).sqrMagnitude > 0.02f)
        {
            Sense();
            // lastPos = transform.position;
        }
    }

    [NaughtyAttributes.Button]
    public void Sense()
    {
        if (!canSense)
            return;
        Ray ray = new Ray(transform.position, transform.forward);
        hitRay.PaintAtRay(ray, false, 1f, 8);
    }

    private void OnDrawGizmos()
    {
        // Gizmos.color = Color.green;
        Ray ray = new Ray(transform.position, transform.forward);
        var hit = default(RaycastHit);

        if (Physics.Raycast(ray, out hit, 0.5f, 1 << 8))
        {
            if (hit.collider.gameObject.layer == 8)
            {
                Gizmos.color = Color.green;
            }
            else
            {
                Debug.Log("Hit layer: " + hit.collider.gameObject.layer + " " + hit.collider.gameObject.name);
                Gizmos.color = Color.red;

            }
        }
        Gizmos.DrawLine(transform.position, transform.position + transform.forward * 0.25f);

    }
}
