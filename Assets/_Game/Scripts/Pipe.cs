﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PaintIn3D;

public class Pipe : MonoBehaviour
{
    public Obi.ObiRope rope;
    /// <summary>
    /// LateUpdate is called every frame, if the Behaviour is enabled.
    /// It is called after all Update functions have been called.
    /// </summary>
    void LateUpdate()
    {
        transform.position = rope.GetParticlePosition(0);
        transform.rotation = rope.GetParticleOrientation(0);
    }
    #region CONST
    #endregion

    #region EDITOR PARAMS

    // public P3dHitCollisions hitCollisions;

    #endregion

    #region PARAMS
    #endregion

    #region PROPERTIES
    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    // private void OnCollisionEnter(Collision other)
    // {
    //     hitCollisions.CheckCollision(other);
    // }

    #endregion

    #region DEBUG
    #endregion

}
