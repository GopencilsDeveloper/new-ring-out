﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Hellmade.Sound;

[ExecuteInEditMode]
public class MapManager : MonoBehaviour
{

    #region CONST
    public const string MAPDEBUG = "MAPDEBUG";
    #endregion

    #region EDITOR PARAMS
    public Transform cameraParent;
    public RopeTeleport ropeTeleport;

    // [NaughtyAttributes.ReorderableList]
    public List<Map> mapsList;
    public List<Map> mapsList2;
    public List<Map> mapsEasyList;
    public List<Map> mapsNormalList;
    public List<Map> mapsHardList;
    public List<Map> mapsHard2List;

    [Space(20)]
    public Transform mapsParent;
    public ParticleSystem confettiPS;
    public ParticleSystem blinkPS;
    #endregion

    #region PARAMS
    // public List<Sprite> artsSpriteList;
    public Map currentMap;
    public Art currentArt;
    public bool isNextLevel;

    public int index;
    #endregion

    #region PROPERTIES
    public static MapManager Instance { get; private set; }
    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void Awake()
    {
        Instance = this;
        // InitMapList();
    }


    private void InitMapList()
    {
        mapsList.Clear();

        mapsList.AddRange(mapsEasyList);
        mapsList.AddRange(mapsNormalList);
        mapsList.AddRange(mapsHardList);
        mapsList.AddRange(mapsHard2List);
    }

    public void Refresh()
    {
        StopAllCoroutines();

        if (currentMap != null)
        {
            Destroy(currentMap.gameObject);
            currentMap = null;
        }

        if (currentArt != null)
        {
            Destroy(currentArt.gameObject);
            currentArt = null;
        }

        // confettiPS.gameObject.SetActive(false);
        blinkPS.gameObject.SetActive(false);
    }


    public int lastIndex;
    [NaughtyAttributes.Button]
    public void SpawnMap()
    {
        isNextLevel = true;
        Refresh();
        index = DataManager.Instance.currentLevel;
        if (index >= mapsList.Count)
        {
            index = (int)Random.Range(0f, mapsList.Count);
        }
        lastIndex = index;

        // Map
        Map map = Instantiate(mapsList[index]);
        // Map map = Instantiate(mapsList[0]);
        map.transform.SetParent(mapsParent);
        currentMap = map;

    }

    public void SpawnMap(int index)
    {
        isNextLevel = false;
        lastIndex = index;
        Refresh();
        //Map
        Map map = Instantiate(mapsList[index]);
        map.transform.SetParent(mapsParent);
        currentMap = map;

    }


    [NaughtyAttributes.Button]
    public void OnMapCompleted(float delayDuration)
    {
        StartCoroutine(C_OnMapCompleted(delayDuration));
    }

    IEnumerator C_OnMapCompleted(float delayDuration)
    {
        if (isNextLevel)
        {
            DataManager.Instance.currentLevel++;
            DataManager.Instance.SaveData();
        }

        yield return new WaitForSeconds(delayDuration);
        currentMap.canRotate = false;
        ropeTeleport.gameObject.SetActive(false);
        cameraParent.transform.DORotate(new Vector3(0f, 180f, -currentMap.startRot.z), 1f, RotateMode.FastBeyond360);
        EazySoundManager.PlaySound(SoundManager.Instance.finishPercent);
        yield return new WaitForSeconds(1.25f);
        EazySoundManager.PlaySound(SoundManager.Instance.rotate2Rounds);
        currentMap.pipe.transform.DOLocalRotate(new Vector3(0f, -720f, 0f), 1.5f, RotateMode.FastBeyond360);
        yield return new WaitForSeconds(1.75f);
        currentMap.gameObject.SetActive(false);
        cameraParent.transform.rotation = Quaternion.Euler(0f, 180f, 0f);
        UIManager.Instance.HideMiniMapUI();

        EazySoundManager.PlaySound(SoundManager.Instance.clap);
        currentMap.SpawnArt();
        yield return new WaitForSeconds(2f);
        

        confettiPS.gameObject.SetActive(true);
        confettiPS.Stop();
        confettiPS.Play();
        yield return new WaitForSeconds(0.5f);

        blinkPS.gameObject.SetActive(true);
        blinkPS.Stop();
        blinkPS.Play();
        GameManager.Instance.WinGame();

        ropeTeleport.transform.position = new Vector3(0f, 0f, 100f);
        ropeTeleport.gameObject.SetActive(false);
        // yield return new WaitForSeconds(0.5f);
        // EazySoundManager.PlaySound(SoundManager.Instance.endWinFX);
    }


    #endregion

    #region DEBUG

    // public static string DEBUGMAP = "DEBUGMAP";
    // public int index1;

    // [NaughtyAttributes.Button]
    // public void NextMap()
    // {
    //     ropeTeleport.gameObject.SetActive(false);
    //     index1 = PlayerPrefs.GetInt(DEBUGMAP);
    //     CameraController.Instance.Refresh();

    //     index1++;
    //     index1 = index1 > mapsList.Count - 1 ? 0 : index1;
    //     PlayerPrefs.SetInt(DEBUGMAP, index1);
    //     // GameManager.Instance.StartGame();
    //     MapManager.Instance.ropeTeleport.gameObject.SetActive(false);
    //     CameraController.Instance.Refresh();
    //     SpawnMap(index1);
    //     GameManager.Instance.PlayGame();
    //     MapManager.Instance.SpawnMap();
    //     currentMap.Active();
    //     currentMap.canRotate = true;
    //     UIManager.Instance.isCompleted = false;
    //     // UIManager.Instance.txtDebugLevel.text = currentMap.mapArtSprite.name;
    //     // UIManager.Instance.imgDebugArt.sprite = currentMap.mapArtSprite;
    //     // currentMap.Active();
    //     // DataManager.Instance.currentLevel = index;
    //     // DataManager.Instance.SaveData();

    // }

    // [NaughtyAttributes.Button]
    // public void PreMap()
    // {
    //     ropeTeleport.gameObject.SetActive(false);
    //     index1 = PlayerPrefs.GetInt(DEBUGMAP);

    //     CameraController.Instance.Refresh();

    //     index1--;
    //     index1 = index1 < 0 ? mapsList.Count - 1 : index1;
    //     PlayerPrefs.SetInt(DEBUGMAP, index1);

    //     // GameManager.Instance.StartGame();
    //     MapManager.Instance.ropeTeleport.gameObject.SetActive(false);
    //     CameraController.Instance.Refresh();
    //     SpawnMap(index1);
    //     GameManager.Instance.PlayGame();
    //     MapManager.Instance.SpawnMap();
    //     currentMap.Active();
    //     currentMap.canRotate = true;
    //     UIManager.Instance.isCompleted = false;
    //     // UIManager.Instance.txtDebugLevel.text = currentMap.mapArtSprite.name;
    //     // UIManager.Instance.imgDebugArt.sprite = currentMap.mapArtSprite;
    //     // currentMap.Active();
    //     // PlayerPrefs.SetInt(DEBUGMAP, index);
    //     // DataManager.Instance.currentLevel = index;
    //     // DataManager.Instance.SaveData();
    // }

    // public List<Map> tempMaps;
    // public GameObject addedSensors;

    // [NaughtyAttributes.Button]
    // public void GetSensors()
    // {
    //     for (int i = 0; i < tempMaps.Count; i++)
    //     {
    //         Scanner scanner = tempMaps[i].scanner;
    //         GameObject addedSensorsInstance = Instantiate(addedSensors);
    //         addedSensorsInstance.transform.SetParent(scanner.transform.GetChild(2));
    //         addedSensorsInstance.transform.localPosition = Vector3.zero;
    //         addedSensorsInstance.transform.localRotation = Quaternion.Euler(0f, 15f, 0f);
    //         Debug.Log("Sen");
    //         scanner.GetSensors();
    //     }
    // }

    // [NaughtyAttributes.Button]
    // public void GetArts()
    // {
    //     artsSpriteList.Clear();
    //     for (int i = 0; i < mapsList.Count; i++)
    //     {
    //         artsSpriteList.Add(mapsList[i].mapArtSprite);
    //     }
    // }

    #endregion

}
