﻿using UnityEngine;
using System.Collections.Generic;
using PaintIn3D;
using Obi;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class P3dRopePaintRay : P3dHitConnector
{
    public enum OrientationType
    {
        WorldUp,
        CameraUp
    }

    public enum NormalType
    {
        HitNormal,
        RayDirection
    }
    public ObiRope rope;

    [SerializeField] private float offset;   /// If you want the raycast hit point to be offset from the surface a bit, this allows you to set by how much in world space.
    public OrientationType Orientation { set { orientation = value; } get { return orientation; } }
    [SerializeField] private OrientationType orientation = OrientationType.CameraUp;

    [SerializeField] private NormalType normal;  /// Which normal should the hit point rotation be based on?

    // Vector3 lastPos1 = Vector3.zero, lastPos2 = Vector3.zero;
    List<Vector3> lastPos;
    public float minSquareDistanceToPaint = 0.02f;

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void OnEnable()
    {
        lastPos = new List<Vector3>();
        for (int i = 0; i < rope.positions.Length; i++)
        {
            lastPos.Add(Vector3.zero);
        }
    }

    [NaughtyAttributes.Button]
    public void Sense()
    {
        // if (!canSense)
        //     return;
        for (int i = 0; i < rope.positions.Length / 2; i++)
        {
            int id = 0 + i;
            int idx = rope.positions.Length / 2 + i;
            var pos = rope.GetParticlePosition(id);
            var pos2 = rope.GetParticlePosition(idx);

            if ((pos - lastPos[id]).sqrMagnitude <= minSquareDistanceToPaint && (pos2 - lastPos[idx]).sqrMagnitude <= minSquareDistanceToPaint) continue;

            lastPos[id] = pos;
            lastPos[idx] = pos2;

            if (pos.y < pos2.y)
            {
                var p = pos;
                pos = pos2;
                pos2 = p;
            }

            //incase the compiler optimized this <true || _DONT_CARE> scenarior
            bool isPainted = PaintByRay(pos, pos2);
            bool isPainted2 = PaintByRay(pos2, pos);
            if (isPainted || isPainted2) break;
        }
        //duplicate paint to avoid missing out corners
        // PaintByRay(pos2, pos);
    }

    private bool PaintByRay(Vector3 from, Vector3 to)
    {
        var distance = to - from;
        var d = distance.magnitude;
        var dir = distance.normalized;
        Ray ray = new Ray(from, dir);
        return PaintAtRay(ray, false, 1f, 8, d);
    }

    private void FixedUpdate()
    {
        // if ((transform.position - lastPos).sqrMagnitude > 0.02f)
        {
            Sense();
            // lastPos = transform.position;
        }
    }

    // private void OnDrawGizmos()
    // {
    //     var pos = rope.GetParticlePosition(0);
    //     var pos2 = rope.GetParticlePosition(rope.positions.Length / 2);
    //     // Gizmos.color = Color.green;
    //     Ray ray = new Ray(pos, (pos2 - pos).normalized);
    //     var hit = default(RaycastHit);

    //     if (Physics.Raycast(ray, out hit, 1, 1 << 8))
    //     {
    //         if (hit.collider.gameObject.layer == 8)
    //         {
    //             Gizmos.color = Color.green;
    //         }
    //         else
    //         {
    //             Debug.Log("Hit layer: " + hit.collider.gameObject.layer + " " + hit.collider.gameObject.name);
    //             Gizmos.color = Color.red;

    //         }
    //     }
    //     Gizmos.DrawLine(transform.position, transform.position + transform.forward * 0.25f);

    // }

    public bool PaintAtRay(Ray ray, bool preview, float pressure, int layer, float rayDistance)
    {
        if (Camera.main != null)
        {
            var hit = default(RaycastHit);

            if (Physics.Raycast(ray, out hit, rayDistance, 1 << layer))
            {
                // if (hit.collider.gameObject.layer == layer)
                {
                    // Debug.Log("Hit: " + hit.collider.gameObject.layer);
                    var finalUp = orientation == OrientationType.CameraUp ? Camera.main.transform.up : Vector3.up;
                    var finalPosition = hit.point + hit.normal * offset;
                    var finalNormal = normal == NormalType.HitNormal ? -hit.normal : ray.direction;
                    var finalRotation = Quaternion.LookRotation(finalNormal, finalUp);

                    hitCache.InvokeRaycast(gameObject, null, null, preview, hit, pressure);

                    DispatchHits(preview, hit.collider, finalPosition, finalRotation, pressure, null);
                    return true;
                }

            }
        }

        return false;
    }

}
