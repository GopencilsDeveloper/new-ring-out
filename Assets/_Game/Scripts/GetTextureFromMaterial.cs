﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetTextureFromMaterial : MonoBehaviour
{
    public Renderer targetToCopyFrom;
    public string fieldToCopyFrom = "_MainTex";
    public Renderer targetToCopyTo;
    public string fieldToCopyTo = "_SliceTex";
    // Start is called before the first frame update
    void OnEnable()
    {
        var tex = targetToCopyFrom.sharedMaterial.GetTexture(fieldToCopyFrom);
        targetToCopyTo.sharedMaterial.SetTexture(fieldToCopyTo, tex);
    }

}
