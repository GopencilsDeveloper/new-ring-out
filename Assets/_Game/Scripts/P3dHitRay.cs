﻿using UnityEngine;
using System.Collections.Generic;
using PaintIn3D;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class P3dHitRay : P3dHitConnector
{
    public enum OrientationType
    {
        WorldUp,
        CameraUp
    }

    public enum NormalType
    {
        HitNormal,
        RayDirection
    }

    [SerializeField] private float offset;   /// If you want the raycast hit point to be offset from the surface a bit, this allows you to set by how much in world space.
    public OrientationType Orientation { set { orientation = value; } get { return orientation; } }
    [SerializeField] private OrientationType orientation = OrientationType.CameraUp;

    [SerializeField] private NormalType normal;  /// Which normal should the hit point rotation be based on?

    public void PaintAtRay(Ray ray, bool preview, float pressure, int layer)
    {
        if (Camera.main != null)
        {
            var hit = default(RaycastHit);

            if (Physics.Raycast(ray, out hit, 0.25f, 1 << layer))
            {
                // if (hit.collider.gameObject.layer == layer)
                {
                    // Debug.Log("Hit: " + hit.collider.gameObject.layer);
                    var finalUp = orientation == OrientationType.CameraUp ? Camera.main.transform.up : Vector3.up;
                    var finalPosition = hit.point + hit.normal * offset;
                    var finalNormal = normal == NormalType.HitNormal ? -hit.normal : ray.direction;
                    var finalRotation = Quaternion.LookRotation(finalNormal, finalUp);

                    hitCache.InvokeRaycast(gameObject, null, null, preview, hit, pressure);

                    DispatchHits(preview, hit.collider, finalPosition, finalRotation, pressure, null);
                    return;
                }

            }
        }
    }

}
