﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArtUI : MonoBehaviour
{
    public Image img;
    public Text txtName;

    public int ID;
    public Sprite spriteArt;
    public string mapName;

    public void Initialize(int ID, Sprite spriteArt, string mapName)
    {
        this.ID = ID;
        this.spriteArt = spriteArt;
        this.mapName = mapName;
    }

    public void Show()
    {
        img.sprite = this.spriteArt;
        txtName.text = this.mapName.ToUpper();
    }

    public void OnClick_ShowPopUp()
    {
        UIManager.Instance.selectedArtUI = this;
        UIManager.Instance.ShowReplayPopup();
    }

    public void OnClick_No()
    {
        UIManager.Instance.HideReplayPopup();
    }

}
