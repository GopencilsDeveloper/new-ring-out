﻿using System.Collections;
using System.Collections.Generic;
using MoreMountains.NiceVibrations;
using PaintIn3D.Examples;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS
    [Header("SCREENS")]
    public GameObject Menu;
    public GameObject InGame;
    public GameObject WinGame;
    public GameObject LoseGame;
    public GameObject Preview;
    public GameObject Gallery;
    public GameObject MiniMap;

    [Header("DEBUG")]
    public Text txtDebugLevel;
    public Image imgDebugArt;

    [Header("MENU")]
    public Button btnGallery;
    public Button btnShop;

    [Header("GALLERY")]
    public GameObject replayPopUp;
    public ArtUI selectedArtUI;
    public Image imgArtUIPlayAgain;

    [Header("WINGAME")]
    public Image imgArt;
    public Button btnReset;


    [Header("PREVIEW")]
    public Image imgArtPreview;

    [Header("INGAME")]
    public Text txtPercent;
    public GameObject level;
    public Text txtLevel;

    [Header("MINIMAP")]
    public RectTransform imgMiniMapBorder;
    #endregion

    #region PARAMS
    #endregion

    #region PROPERTIES
    public static UIManager Instance { get; private set; }
    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void Awake()
    {
        Instance = this;
    }

    private void OnEnable()
    {
        RegisterEvents();
    }

    private void OnDisable()
    {
        UnregisterEvents();
    }

    private void RegisterEvents()
    {
        GameManager.OnStateChanged += OnGameStateChanged;
    }

    private void UnregisterEvents()
    {
        GameManager.OnStateChanged -= OnGameStateChanged;
    }

    public void OnGameStateChanged(GameState state)
    {
        switch (state)
        {
            case GameState.MENU:
                OnMenu();
                break;
            case GameState.INGAME:
                OnInGame();
                break;
            case GameState.WINGAME:
                OnWinGame();
                break;
            case GameState.LOSEGAME:
                OnLoseGame();
                break;
        }
    }

    public void OnMenu()
    {
        Menu.SetActive(true);
        InGame.SetActive(true);
        WinGame.SetActive(false);
        LoseGame.SetActive(false);
        Preview.SetActive(false);
        txtLevel.text = (DataManager.Instance.currentLevel + 1).ToString();
        HideMiniMapUI();
        txtPercent.text = "";
        btnReset.gameObject.SetActive(false);
        isCompleted = false;
        isVibrated = false;
    }

    public void OnInGame()
    {
        Menu.SetActive(false);
        InGame.SetActive(true);
        WinGame.SetActive(false);
        LoseGame.SetActive(false);

        ShowLevel();
        txtLevel.text = (DataManager.Instance.currentLevel + 1).ToString();
        ShowMiniMapUI();
        btnReset.gameObject.SetActive(true);

        ShowPreview();
    }

    public void OnWinGame()
    {
        Menu.SetActive(false);
        WinGame.SetActive(true);
        LoseGame.SetActive(false);
        Preview.SetActive(false);
        btnReset.gameObject.SetActive(false);
        HideMiniMapUI();
    }

    public void OnLoseGame()
    {
        Menu.SetActive(false);
        WinGame.SetActive(false);
        LoseGame.SetActive(true);
        Preview.SetActive(false);
        btnReset.gameObject.SetActive(false);
    }

    public void ShowPreview()
    {
        StartCoroutine(C_ShowPreview());
    }

    IEnumerator C_ShowPreview()
    {
        Preview.SetActive(true);
        yield return new WaitForSeconds(GameConfiguration.Instance.delayRotate);
        Preview.SetActive(false);
    }

    private void Update()
    {
        if (GameManager.Instance.currentState == GameState.INGAME)
        {
            CheckPercentage();
        }
    }

    public bool isCompleted;
    public bool isVibrated;
    public void CheckPercentage()
    {
        float ratio = P3dChannelCounter.GetRatioRGBA().w;
        var percentage = Mathf.Clamp01(ratio) * 100.0f;

        if (percentage > 0f)
        {
            if ((int)percentage % 15f == 0)
            {
                if (!isVibrated)
                {
                    MMVibrationManager.VibrateLight();
                    isVibrated = true;
                }
            }
            else
            {
                isVibrated = false;
            }
        }

        if (percentage < 1f)
        {
            txtPercent.text = "";
        }
        else
        {
            txtPercent.text = string.Format("{0:#}%", percentage);
        }
        if (percentage >= 95f && !isCompleted)
        {
            MapManager.Instance.OnMapCompleted(0.5f);
            isCompleted = true;
        }
    }

    public List<ArtUI> artUIsList = new List<ArtUI>();

    public void ClearGallery()
    {
        if (artUIsList.Count <= 0)
            return;

        for (int i = artUIsList.Count - 1; i >= 0; i--)
        {
            ArtUIPool.Instance.ReturnToPool(artUIsList[i]);
        }
    }

    public void OnClick_btnGallery()
    {
        replayPopUp.SetActive(false);
        ClearGallery();
        for (int i = 0; i < DataManager.Instance.currentLevel; i++)
        {
            if (i >= MapManager.Instance.mapsList.Count)
                return;

            ArtUI artUI = ArtUIPool.Instance.GetFromPool();
            Sprite sprite = MapManager.Instance.mapsList[i].mapArtSprite;
            string mapName = MapManager.Instance.mapsList[i].mapName;

            artUI.Initialize(i, sprite, mapName);
            artUIsList.Add(artUI);
            artUI.Show();
        }

    }

    public void ShowReplayPopup()
    {
        replayPopUp.gameObject.SetActive(true);
        imgArtUIPlayAgain.sprite = selectedArtUI.spriteArt;


        bool isXBigger = selectedArtUI.spriteArt.bounds.extents.x > selectedArtUI.spriteArt.bounds.extents.y ? true : false;

        if (isXBigger)
        {
            imgArtUIPlayAgain.transform.localScale = new Vector3(1f, selectedArtUI.spriteArt.bounds.extents.y / selectedArtUI.spriteArt.bounds.extents.x, 1f);
        }
        else
        {
            imgArtUIPlayAgain.transform.localScale = new Vector3(selectedArtUI.spriteArt.bounds.extents.x / selectedArtUI.spriteArt.bounds.extents.y, 1, 1f);
        }
    }

    public void HideReplayPopup()
    {
        replayPopUp.gameObject.SetActive(false);
    }

    public void HideGallery()
    {
        Gallery.SetActive(false);
    }

    public void ShowLevel()
    {
        level.SetActive(true);
    }

    public void HideLevel()
    {
        level.SetActive(false);
    }

    public void OnClick_Yes()
    {
        HideGallery();
        MapManager.Instance.ropeTeleport.gameObject.SetActive(false);
        MapManager.Instance.SpawnMap(selectedArtUI.ID);
        MapManager.Instance.currentMap.Active();
        MapManager.Instance.currentMap.canRotate = true;
        GameManager.Instance.PlayGame();
        UIManager.Instance.HideLevel();
    }

    public void ShowMiniMapUI()
    {
        MiniMap.SetActive(true);
        SetMinimapPosition();
    }

    public void HideMiniMapUI()
    {
        MiniMap.SetActive(false);
    }

    public void SetMinimapPosition()
    {
        Vector2 pos;
        Vector2 scale;
        if (Camera.main.aspect <= 0.475f)
        {
            pos = new Vector2(0f, -30f);
            scale = new Vector2(1.25f, 1.25f);
        }
        else
        {
            pos = Vector3.zero;
            scale = Vector2.one;
        }
        imgMiniMapBorder.anchoredPosition = pos;
        imgMiniMapBorder.localScale = scale;
    }


    #endregion
}