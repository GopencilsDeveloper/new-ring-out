﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(MeshFilter))]
public class ConverseMesh : MonoBehaviour
{
    MeshFilter meshFilter;

    private void Awake()
    {
        meshFilter = GetComponent<MeshFilter>();
    }

    [NaughtyAttributes.Button]
    public void Converse()
    {
        meshFilter.mesh.triangles = meshFilter.mesh.triangles.Reverse().ToArray();
    }

    [NaughtyAttributes.Button]
    public void SaveMesh()
    {
        var savePath = "Assets/_Game/Models/DOING/Conversed/" + gameObject.name + ".asset";
#if UNITY_EDITOR
        AssetDatabase.CreateAsset(meshFilter.mesh, savePath);
        AssetDatabase.SaveAssets();
#endif
    }

    [NaughtyAttributes.Button]
    public void ReloadMeshCollider()
    {
        MeshCollider meshCollider = GetComponent<MeshCollider>();
        if (meshCollider != null)
        {
            DestroyImmediate(meshCollider);
        }
        gameObject.AddComponent<MeshCollider>();

    }


    // public List<Mesh> meshesList;

    // [NaughtyAttributes.Button]
    // public void ConverseAll()
    // {
    //     for (int i = 0; i < meshesList.Count; i++)
    //     {
    //         Converse(i);
    //     }
    // }

    // public void Converse(int index)
    // {
    //     Mesh mesh = meshesList[index];

    //     MeshFilter meshFilter = gameObject.AddComponent<MeshFilter>();
    //     meshFilter.sharedMesh = mesh;

    //     meshFilter.sharedMesh.triangles = meshFilter.sharedMesh.triangles.Reverse().ToArray();

    //     var savePath = "Assets/_Game/Models/Conversed/" + "Conversed_" + index + ".asset";
    //     AssetDatabase.CreateAsset(meshFilter.sharedMesh, savePath);
    //     AssetDatabase.SaveAssets();

    //     DestroyImmediate(meshFilter);
    // }
}
