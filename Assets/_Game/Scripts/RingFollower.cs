using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Obi;
using System;

public class RingFollower : MonoBehaviour
{
    public ObiRope rope;
    private Transform tf;
    // Start is called before the first frame update
    void Start()
    {
        tf = transform;
    }

    // Update is called once per frame
    void Update()
    {
        if(rope != null){
            Follow();
        }
    }

    private void Follow()
    {
        var pos = rope.GetParticlePosition(0);
        var pos2 = rope.GetParticlePosition(rope.positions.Length / 2);
        pos = (pos + pos2)/2;
        tf.position = pos;
    }
}
