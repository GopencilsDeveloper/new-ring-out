﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : MonoBehaviour
{
    #region CONST
    public const string LEVEL = "LEVEL";
    public const string SCORE = "SCORE";
    #endregion

    #region EDITOR PARAMS
    #endregion

    #region PARAMS
    public int currentLevel;
    public int currentScore;
    #endregion

    #region PROPERTIES
    public static DataManager Instance { get; private set; }
    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void Awake()
    {
        Instance = this;
        // #if UNITY_EDITOR
        // ResetData();
        // #else
        LoadData();
        // #endif
        // PlayerPrefs.SetInt(LEVEL, 300);
        // currentLevel = PlayerPrefs.GetInt(LEVEL);
    }

    public void LoadData()
    {
        currentLevel = PlayerPrefs.GetInt(LEVEL);
        currentScore = PlayerPrefs.GetInt(SCORE);
    }

    public void SaveData()
    {
        PlayerPrefs.SetInt(LEVEL, currentLevel);
        PlayerPrefs.SetInt(SCORE, currentScore);
    }

    public void ResetData()
    {
        PlayerPrefs.SetInt(LEVEL, 0);
        PlayerPrefs.SetInt(SCORE, 0);
        SaveData();
    }
    #endregion
}
