﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public static CameraController Instance { get; private set; }
    public Camera cameraMain;

    private void Awake()
    {
        Instance = this;
    }

    public void Refresh()
    {
        transform.rotation = Quaternion.identity;
        InputManager.Instance.Reset();
    }

    private void Start()
    {
        UpdateFOV();
    }

    public void UpdateFOV()
    {
        float aspect = cameraMain.aspect;
        if (aspect <= 0.475f)    //9:19  
        {
            Debug.Log("9:19  ");
            cameraMain.fieldOfView = 70f;
        }
        else
        if (aspect <= 0.5625f)    //9:16  
        {
            cameraMain.fieldOfView = 60f;
        }
        else
        if (aspect <= 0.75f)    //3:4
        {
            cameraMain.fieldOfView = 50f;
        }
    }
}
