﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PaintIn3D;


public class Conv : MonoBehaviour
{
    public List<GameObject> fbxes;
    public List<P3dSeamFixer> seamFixers;

    [NaughtyAttributes.Button]
    void Converse()
    {
        for (int i = 0; i < fbxes.Count; i++)
        {
            GameObject go = new GameObject(fbxes[i].name);
            go.AddComponent<MeshFilter>().mesh = fbxes[i].GetComponent<MeshFilter>().sharedMesh;
            go.AddComponent<ConverseMesh>();
            go.GetComponent<ConverseMesh>().Converse();
            go.GetComponent<ConverseMesh>().SaveMesh();
        }
    }

    [NaughtyAttributes.Button]
    void Fix()
    {
        for (int i = 0; i < seamFixers.Count; i++)
        {
           seamFixers[i].Generate();
        }
    }
}
