﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState
{
    NULL, MENU, INGAME, WINGAME, LOSEGAME
}

public class GameManager : MonoBehaviour
{
    #region CONST
    #endregion

    #region EDITOR PARAMS
    #endregion

    #region PARAMS
    public GameState currentState;

    public float startTimer;
    public float levelDuration;

    #endregion

    #region PROPERTIES
    public static GameManager Instance { get; private set; }
    #endregion

    #region EVENTS
    public static event System.Action<GameState> OnStateChanged;
    #endregion

    #region METHODS

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        Application.targetFrameRate = 300;
        StartGame();
    }

    public void StartGame()
    {
        ChangeState(GameState.MENU);
        CameraController.Instance.Refresh();
        MapManager.Instance.SpawnMap();
        MapManager.Instance.currentMap.Active();
        ThemeManager.Instance.RandomTheme();

    }

    public void PlayGame()
    {
        ChangeState(GameState.INGAME);
        UIManager.Instance.isCompleted = false;
        MapManager.Instance.currentMap.canRotate = true;
        TinySauce.OnGameStarted(DataManager.Instance.currentLevel.ToString());

        startTimer = Time.unscaledTime;
    }

    public void RestartGame()
    {
        // ChangeState(GameState.INGAME);
        // MapManager.Instance.currentMap.canRotate = true;
        // UIManager.Instance.isCompleted = false;
        // TinySauce.OnGameStarted(DataManager.Instance.currentLevel.ToString());

        MapManager.Instance.ropeTeleport.gameObject.SetActive(false);
        CameraController.Instance.Refresh();
        MapManager.Instance.SpawnMap(MapManager.Instance.lastIndex);
        MapManager.Instance.currentMap.Active();

        PlayGame();

    }

    public void WinGame()
    {
        ChangeState(GameState.WINGAME);
        TinySauce.OnGameFinished(DataManager.Instance.currentLevel.ToString(), true, GetLevelDuration());
    }

    public void LoseGame()
    {
        ChangeState(GameState.LOSEGAME);
        TinySauce.OnGameFinished(DataManager.Instance.currentLevel.ToString(), false, GetLevelDuration());
    }

    public void ChangeState(GameState state)
    {
        currentState = state;
        if (OnStateChanged != null)
        {
            OnStateChanged(state);
        }
    }

    public float GetLevelDuration()
    {
        levelDuration = Time.unscaledTime - startTimer;
        startTimer = Time.unscaledTime;
        // Debug.Log("levelDuration: " + levelDuration);
        return levelDuration;
    }

    #endregion
}
