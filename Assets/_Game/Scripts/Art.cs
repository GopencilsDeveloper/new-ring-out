﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Hellmade.Sound;

public class Art : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS
    public SpriteRenderer spriteRenderer;
    public GameObject flashGO;
    #endregion

    #region PARAMS
    public List<Transform> piecesList = new List<Transform>();
    public Sprite sprite;
    public Explodable explodable;
    public PolygonCollider2D poly;
    public Rigidbody2D rgbd2D;

    bool isScrambling;
    #endregion

    #region PROPERTIES
    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void Awake()
    {
        DOTween.SetTweensCapacity(500, 50);
    }

    [NaughtyAttributes.Button]
    public void Initialize(Sprite sprite)
    {
        this.sprite = sprite;
        spriteRenderer.sprite = sprite;

        poly = gameObject.AddComponent<PolygonCollider2D>();
        rgbd2D = gameObject.AddComponent<Rigidbody2D>();
        rgbd2D.bodyType = RigidbodyType2D.Kinematic;
        explodable = gameObject.AddComponent<Explodable>();
        explodable.extraPoints = 50;
        flashGO.GetComponent<SpriteMask>().sprite = sprite;
    }

    public void GetPiecesList()
    {
        piecesList.Clear();
        foreach (Transform piece in transform)
        {
            piecesList.Add(piece);
        }
    }

    [NaughtyAttributes.Button]
    public void ScramblePieces()
    {
        GetPiecesList();
        if (!isScrambling)
        {
            StartCoroutine(C_ScramblePieces(2.5f));
        }
    }

    IEnumerator C_ScramblePieces(float duration)
    {
        spriteRenderer.enabled = false;
        isScrambling = true;

        List<Vector3> startPosList = new List<Vector3>();
        List<Vector3> endPosList = new List<Vector3>();

        for (int i = 0; i < piecesList.Count; i++)
        {
            Transform piece = piecesList[i];
            Vector3 startPos = piece.position;
            Vector3 endPos = startPos + (Vector3)Random.insideUnitCircle * 20f * (Random.value < 0.5f ? -1f : 1f);

            startPosList.Add(startPos);
            endPosList.Add(endPos);
        }

        //Explode
        for (int i = 0; i < piecesList.Count; i++)
        {
            Transform piece = piecesList[i];
            piece.position = Vector3.zero;
            piece.DOMove(endPosList[i], duration * 0.5f).SetEase(Ease.OutSine);
        }
        yield return new WaitForSeconds(duration * 0.5f);

        //Condense
        for (int i = 0; i < piecesList.Count; i++)
        {
            Transform piece = piecesList[i];
            piece.DOMove(startPosList[i], duration * 0.5f).SetEase(Ease.InOutQuart);
        }
        yield return new WaitForSeconds(duration * 0.5f);
        EazySoundManager.PlaySound(SoundManager.Instance.finishPercent);


        isScrambling = false;

        spriteRenderer.enabled = true;

        flashGO.SetActive(true);
        explodable.deleteFragments();

    }

    [NaughtyAttributes.Button]
    public void OnMapCompleted()
    {
        StartCoroutine(C_OnMapCompleted());
    }

    IEnumerator C_OnMapCompleted()
    {
        explodable.generateFragments(false);
        ScramblePieces();
        yield return null;
    }

    #endregion

    #region DEBUG
    #endregion

}
