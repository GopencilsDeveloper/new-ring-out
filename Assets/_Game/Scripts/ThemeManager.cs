﻿using System.Collections;
using System.Collections.Generic;
using PaintIn3D;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class RopeVisual
{
    public Material ropeMat;
    public Color paintColor;
}

[System.Serializable]
public class Theme
{
    public Sprite backGround;
    public RopeVisual[] ropeVisuals;

}

public class ThemeManager : MonoBehaviour
{
    public static ThemeManager Instance { get; private set; }

    [Header("References")]
    public Image uiImage;
    public MeshRenderer ropeMeshRenderer;
    public P3dPaintSphere paintSphere;

    [Header("Input")]
    public List<Theme> themesList;

    [Header("Params")]
    public Theme currentTheme;
    public RopeVisual currentRopeVisual;

    private void Awake()
    {
        Instance = this;
    }

    public void RandomTheme()
    {
        currentTheme = themesList[(int)Random.Range(0f, themesList.Count)];

        currentRopeVisual = currentTheme.ropeVisuals[(int)Random.Range(0f, currentTheme.ropeVisuals.Length)];

        uiImage.sprite = currentTheme.backGround;
        paintSphere.Color = currentRopeVisual.paintColor;
        ropeMeshRenderer.material = currentRopeVisual.ropeMat;
    }

}
