﻿Shader "Custom/AlphaMask" {
    Properties {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Main Texture (RGB)", 2D) = "white" {}
        _SliceTex("Slice Texture (RGB)", 2D) = "white" {}
        
        _EmissionAmount("Emission amount", float) = 2.0
    }
    SubShader {
        Tags { "RenderType"="Opaque" }
        LOD 200
        Cull Off
        CGPROGRAM
        #pragma surface surf Standard
        #pragma target 3.0
        #define IF(a, b, c) lerp(b, c, step((fixed) (a), 0));
 
        fixed4 _Color;
        sampler2D _MainTex;
        sampler2D _SliceTex;
        
        float _EmissionAmount;
 
        struct Input {
            float2 uv_MainTex;
        };
 
 
        void surf (Input IN, inout SurfaceOutputStandard o) {
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
            float test = tex2D(_SliceTex, IN.uv_MainTex).a;
            test = IF(test > 0, c.a, -1);
            clip(test);
            
            // if (test < _BurnSize && _SliceAmount > 0) {
            //     o.Emission = tex2D(_BurnRamp, float2(test * (1 / _BurnSize), 0)) * _BurnColor * _EmissionAmount;
            // }
 
            o.Albedo = c.rgb;
            // o.Alpha = c.a;
            o.Emission = _EmissionAmount;
            o.Alpha = test;
        }
        ENDCG
    }
    FallBack "Diffuse"
}